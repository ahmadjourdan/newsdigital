import { useRouter } from 'next/router'
import Link from 'next/link'

export default function Header() {
  return (
    <>
      <header className="tech-header header">
        <div className="container-fluid">
          <nav className="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
            <button
              className="navbar-toggler navbar-toggler-right"
              type="button"
              data-toggle="collapse"
              data-target="#navbarCollapse"
              aria-controls="navbarCollapse"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <Link href="/">
              <a className="navbar-brand">
                <img src="images/version/tech-logo.png" />
              </a>
            </Link>
            <div className="collapse navbar-collapse" id="navbarCollapse">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link href="/">
                    <a className="nav-link">Home</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/gadgets">
                    <a className="nav-link">Gadgets</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="tech-category-02.html">
                    Videos
                  </a>
                </li>
                <li className="nav-item">
                  <Link href="/reviews">
                    <a className="nav-link">Reviews</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/contactus">
                    <a className="nav-link">Contact Us</a>
                  </Link>
                </li>
              </ul>
              <ul className="navbar-nav mr-2">
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fa fa-rss" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fa fa-android" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fa fa-apple" />
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
    </>
  )
}
