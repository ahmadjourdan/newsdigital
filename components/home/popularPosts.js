import Link from 'next/link'

export default function PopularPosts({ popularPosts }) {
  return (
    <div className="widget">
      <h2 className="widget-">Popular Posts</h2>
      <div className="blog-list-widget">
        <div className="list-group">
          {popularPosts.articles.slice(0, 5).map((data, index) => (
            <a
              href="tech-single.html"
              className="list-group-item list-group-item-action flex-column align-items-start"
              key={index}
            >
              <div className="w-100 justify-content-between">
                <img src={data.urlToImage} className="img-fluid float-left" />
                <h5 className="mb-1">
                  <Link
                    href={{
                      pathname: '/articles/[id]',
                      query: {
                        detail: JSON.stringify(data),
                        breadcrumb: 'Home',
                      },
                    }}
                    as={`articles/${data.title}`}
                  >
                    {data.title}
                  </Link>
                </h5>
                <small>{data.publishedAt}</small>
              </div>
            </a>
          ))}
        </div>
      </div>
    </div>
  )
}
