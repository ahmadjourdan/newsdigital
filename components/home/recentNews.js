import Link from 'next/link'

export default function RecentNews({ recentNews }) {
  return (
    <>
      <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div className="page-wrapper">
          <div className="blog-top clearfix">
            <h4 className="pull-left">
              Recent News
              <a href="#">
                <i className="fa fa-rss" />
              </a>
            </h4>
          </div>
          <div className="blog-list clearfix">
            {recentNews.articles.map((data, index) => (
              <div className="blog-box row" key={index}>
                <div className="col-md-4">
                  <div className="post-media">
                    <a href="tech-single.html">
                      <img src={data.urlToImage} className="img-fluid" />
                      <div className="hovereffect" />
                    </a>
                  </div>
                </div>
                <div className="blog-meta big-meta col-md-8">
                  <h4>
                    <Link
                      href={{
                        pathname: '/articles/[id]',
                        query: {
                          detail: JSON.stringify(data),
                          breadcrumb: 'Home',
                        },
                      }}
                      as={`articles/${index}`}
                    >
                      <a>{data.title}</a>
                    </Link>
                  </h4>
                  <p>{data.description}</p>
                  <small className="firstsmall">
                    <a className="bg-orange" href="tech-category-01.html">
                      Gadgets
                    </a>
                  </small>
                  <small>
                    <a href="tech-single.html">{data.publishedAt}</a>
                  </small>
                  <small>
                    <a href="tech-author.html">by {data.author}</a>
                  </small>
                  <small>
                    <a href="tech-single.html">
                      <i className="fa fa-eye" /> 1114
                    </a>
                  </small>
                </div>
              </div>
            ))}
          </div>
        </div>
        <hr className="invis" />
        <div className="row">
          <div className="col-md-12">
            <nav aria-label="Page navigation">
              <ul className="pagination justify-content-start">
                <li className="page-item">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    Next
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </>
  )
}
