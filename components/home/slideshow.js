import Link from 'next/link'

export default function SlideShow({ slideShow }) {
  return (
    <>
      <section className="section first-section">
        <div className="container-fluid">
          <div className="masonry-blog clearfix">
            {slideShow.articles.slice(1, 3).map((data, index) => (
              <div className="first-slot" key={index}>
                <div className="masonry-box post-media">
                  <img src={data.urlToImage} className="img-fluid" />
                  <div className="shadoweffect">
                    <div className="shadow-desc">
                      <div className="blog-meta">
                        <span className="bg-orange">
                          <a href="tech-category-01.html">Technology</a>
                        </span>
                        <h4>
                          <Link
                            href={{
                              pathname: '/articles/[id]',
                              query: {
                                detail: JSON.stringify(data),
                                breadcrumb: 'Home',
                              },
                            }}
                            as={`articles/${data.title}`}
                          >
                            <a>{data.title}</a>
                          </Link>
                        </h4>
                        <small>
                          <a href="tech-single.html">{data.publishedAt}</a>
                        </small>
                        <small>
                          <a href="tech-author.html">by {data.author}</a>
                        </small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  )
}
