export default function TrendVideos() {
  return (
    <div className="widget">
      <h2 className="widget-">Trend Videos</h2>
      <div className="trend-videos">
        <div className="blog-box">
          <div className="post-media">
            <a href="tech-single.html">
              <img src="upload/tech_video_01.jpg" className="img-fluid" />
              <div className="hovereffect">
                <span className="videohover" />
              </div>
            </a>
          </div>
          <div className="blog-meta">
            <h4>
              <a href="tech-single.html">
                We prepared the best 10 laptop presentations for you
              </a>
            </h4>
          </div>
        </div>
        <hr className="invis" />
        <div className="blog-box">
          <div className="post-media">
            <a href="tech-single.html">
              <img src="upload/tech_video_02.jpg" className="img-fluid" />
              <div className="hovereffect">
                <span className="videohover" />
              </div>
            </a>
          </div>
          <div className="blog-meta">
            <h4>
              <a href="tech-single.html">
                We are guests of ABC Design Studio - Vlog
              </a>
            </h4>
          </div>
        </div>
        <hr className="invis" />
        <div className="blog-box">
          <div className="post-media">
            <a href="tech-single.html">
              <img src="upload/tech_video_03.jpg" className="img-fluid" />
              <div className="hovereffect">
                <span className="videohover" />
              </div>
            </a>
          </div>
          <div className="blog-meta">
            <h4>
              <a href="tech-single.html">
                Both blood pressure monitor and intelligent clock
              </a>
            </h4>
          </div>
        </div>
      </div>
    </div>
  )
}
