import Head from 'next/head'
import Header from './header'
import Footer from './footer'
export default function Layout({ children }) {
  return (
    <>
      <meta charSet="utf-8" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <title>Tech Blog</title>
      <meta name="keywords" content />
      <meta name="description" content />
      <meta name="author" content />

      <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
      <link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
      <link
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"
        rel="stylesheet"
      />
      <link href="css/bootstrap.css" rel="stylesheet" />
      <link href="css/font-awesome.min.css" rel="stylesheet" />
      <link href="style.css" rel="stylesheet" />
      <link href="css/responsive.css" rel="stylesheet" />
      <link href="css/colors.css" rel="stylesheet" />
      <link href="css/version/tech.css" rel="stylesheet" />

      <div id="wrapper">
        {/* Header */}
        <Header />
        {/* End Header */}

        {children}

        {/* Footer */}
        <Footer />
        {/* End Footer */}
        <div className="dmtop">Scroll to Top</div>
      </div>
      <script src="js/jquery.min.js"></script>
      <script src="js/tether.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/custom.js"></script>
    </>
  )
}
