import Link from 'next/link'
import { useRouter } from 'next/router'

export default function ArticleDetail() {
  const router = useRouter()
  const breadcrumb = router.query.breadcrumb
  const detail = JSON.parse(router.query.detail)
  return (
    <>
      <section className="section single-wrapper">
        <div className="container">
          <div className="row">
            <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
              <div className="page-wrapper">
                <div className="blog-title-area text-center">
                  <ol className="breadcrumb hidden-xs-down">
                    <li className="breadcrumb-item">
                      <Link href="/">
                        <a>{breadcrumb}</a>
                      </Link>
                    </li>
                    <li className="breadcrumb-item active">{detail.title}</li>
                  </ol>
                  <span className="color-orange">
                    <a href="tech-category-01.html">Technology</a>
                  </span>
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="banner-spot clearfix">
                        <div className="banner-img">
                          <img src={detail.urlToImage} className="img-fluid" />
                        </div>
                        {/* end banner-img */}
                      </div>
                      {/* end banner */}
                    </div>
                    {/* end col */}
                  </div>
                  {/* end row */}
                  <h3>{detail.title}</h3>
                  <div className="blog-meta big-meta">
                    <small>{detail.publishedAt}</small>
                    <small>by {detail.author}</small>
                    <small>
                      <i className="fa fa-eye" /> 2344
                    </small>
                  </div>
                  {/* end meta */}
                  <div className="post-sharing">
                    <ul className="list-inline">
                      <li>
                        <a href="#" className="fb-button btn btn-primary">
                          <i className="fa fa-facebook" />{' '}
                          <span className="down-mobile">Share on Facebook</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" className="tw-button btn btn-primary">
                          <i className="fa fa-twitter" />{' '}
                          <span className="down-mobile">Tweet on Twitter</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" className="gp-button btn btn-primary">
                          <i className="fa fa-google-plus" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  {/* end post-sharing */}
                </div>
                {/* end title */}
                {/* end media */}
                <div className="blog-content">
                  <div className="pp">
                    <p>{detail.description}</p>
                  </div>
                  {/* end pp */}
                </div>
                {/* end content */}
                <div className="blog-title-area">
                  <div className="tag-cloud-single">
                    <span>Tags</span>
                    <small>
                      <a href="#">lifestyle</a>
                    </small>
                    <small>
                      <a href="#">colorful</a>
                    </small>
                    <small>
                      <a href="#">trending</a>
                    </small>
                    <small>
                      <a href="#">another tag</a>
                    </small>
                  </div>
                  {/* end meta */}
                  <div className="post-sharing">
                    <ul className="list-inline">
                      <li>
                        <a href="#" className="fb-button btn btn-primary">
                          <i className="fa fa-facebook" />{' '}
                          <span className="down-mobile">Share on Facebook</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" className="tw-button btn btn-primary">
                          <i className="fa fa-twitter" />{' '}
                          <span className="down-mobile">Tweet on Twitter</span>
                        </a>
                      </li>
                      <li>
                        <a href="#" className="gp-button btn btn-primary">
                          <i className="fa fa-google-plus" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  {/* end post-sharing */}
                </div>
                {/* end title */}
                <hr className="invis1" />
                <div className="custombox authorbox clearfix">
                  <h4 className="small-title">About author</h4>
                  <div className="row">
                    <div className="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                      <img
                        src="upload/author.jpg"
                        alt
                        className="img-fluid rounded-circle"
                      />
                    </div>
                    {/* end col */}
                    <div className="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                      <h4>{detail.author}</h4>
                      <p>
                        Quisque sed tristique felis. Lorem
                        <a href="#">visit my website</a> amet, consectetur
                        adipiscing elit. Phasellus quis mi auctor, tincidunt
                        nisl eget, finibus odio. Duis tempus elit quis risus
                        congue feugiat. Thanks for stop Tech Blog!
                      </p>
                      <div className="topsocial">
                        <a
                          href="#"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Facebook"
                        >
                          <i className="fa fa-facebook" />
                        </a>
                        <a
                          href="#"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Youtube"
                        >
                          <i className="fa fa-youtube" />
                        </a>
                        <a
                          href="#"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Pinterest"
                        >
                          <i className="fa fa-pinterest" />
                        </a>
                        <a
                          href="#"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Twitter"
                        >
                          <i className="fa fa-twitter" />
                        </a>
                        <a
                          href="#"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Instagram"
                        >
                          <i className="fa fa-instagram" />
                        </a>
                        <a
                          href="#"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Website"
                        >
                          <i className="fa fa-link" />
                        </a>
                      </div>
                      {/* end social */}
                    </div>
                    {/* end col */}
                  </div>
                  {/* end row */}
                </div>
                {/* end author-box */}
                <hr className="invis1" />
                <hr className="invis1" />
                <div className="custombox clearfix">
                  <h4 className="small-title">3 Comments</h4>
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="comments-list">
                        <div className="media">
                          <a className="media-left" href="#">
                            <img
                              src="upload/author.jpg"
                              alt
                              className="rounded-circle"
                            />
                          </a>
                          <div className="media-body">
                            <h4 className="media-heading user_name">
                              Amanda Martines <small>5 days ago</small>
                            </h4>
                            <p>
                              Exercitation photo booth stumptown tote bag
                              Banksy, elit small batch freegan sed. Craft beer
                              elit seitan exercitation, photo booth et 8-bit
                              kale chips proident chillwave deep v laborum.
                              Aliquip veniam delectus, Marfa eiusmod Pinterest
                              in do umami readymade swag. Selfies iPhone
                              Kickstarter, drinking vinegar jean.
                            </p>
                            <a href="#" className="btn btn-primary btn-sm">
                              Reply
                            </a>
                          </div>
                        </div>
                        <div className="media">
                          <a className="media-left" href="#">
                            <img
                              src="upload/author_01.jpg"
                              alt
                              className="rounded-circle"
                            />
                          </a>
                          <div className="media-body">
                            <h4 className="media-heading user_name">
                              Baltej Singh <small>5 days ago</small>
                            </h4>
                            <p>
                              Drinking vinegar stumptown yr pop-up artisan sunt.
                              Deep v cliche lomo biodiesel Neutra selfies.
                              Shorts fixie consequat flexitarian four loko
                              tempor duis single-origin coffee. Banksy, elit
                              small.
                            </p>
                            <a href="#" className="btn btn-primary btn-sm">
                              Reply
                            </a>
                          </div>
                        </div>
                        <div className="media last-child">
                          <a className="media-left" href="#">
                            <img
                              src="upload/author_02.jpg"
                              alt
                              className="rounded-circle"
                            />
                          </a>
                          <div className="media-body">
                            <h4 className="media-heading user_name">
                              Marie Johnson <small>5 days ago</small>
                            </h4>
                            <p>
                              Kickstarter seitan retro. Drinking vinegar
                              stumptown yr pop-up artisan sunt. Deep v cliche
                              lomo biodiesel Neutra selfies. Shorts fixie
                              consequat flexitarian four loko tempor duis
                              single-origin coffee. Banksy, elit small.
                            </p>
                            <a href="#" className="btn btn-primary btn-sm">
                              Reply
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* end col */}
                  </div>
                  {/* end row */}
                </div>
                {/* end custom-box */}
                <hr className="invis1" />
                <div className="custombox clearfix">
                  <h4 className="small-title">Leave a Reply</h4>
                  <div className="row">
                    <div className="col-lg-12">
                      <form className="form-wrapper">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Your name"
                        />
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Email address"
                        />
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Website"
                        />
                        <textarea
                          className="form-control"
                          placeholder="Your comment"
                          defaultValue={''}
                        />
                        <button type="submit" className="btn btn-primary">
                          Submit Comment
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* end page-wrapper */}
            </div>
            {/* end col */}
            <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12">
              <div className="sidebar">
                <div className="widget">
                  <div className="banner-spot clearfix">
                    <div className="banner-img">
                      <img src="upload/banner_07.jpg" className="img-fluid" />
                    </div>
                    {/* end banner-img */}
                  </div>
                  {/* end banner */}
                </div>
                {/* end widget */}
                <div className="widget">
                  <h2 className="widget-title">Trend Videos</h2>
                  <div className="trend-videos">
                    <div className="blog-box">
                      <div className="post-media">
                        <a href="tech-single.html">
                          <img
                            src="upload/tech_video_01.jpg"
                            alt
                            className="img-fluid"
                          />
                          <div className="hovereffect">
                            <span className="videohover" />
                          </div>
                          {/* end hover */}
                        </a>
                      </div>
                      {/* end media */}
                      <div className="blog-meta">
                        <h4>
                          <a href="tech-single.html">
                            We prepared the best 10 laptop presentations for you
                          </a>
                        </h4>
                      </div>
                      {/* end meta */}
                    </div>
                    {/* end blog-box */}
                    <hr className="invis" />
                    <div className="blog-box">
                      <div className="post-media">
                        <a href="tech-single.html">
                          <img
                            src="upload/tech_video_02.jpg"
                            alt
                            className="img-fluid"
                          />
                          <div className="hovereffect">
                            <span className="videohover" />
                          </div>
                          {/* end hover */}
                        </a>
                      </div>
                      {/* end media */}
                      <div className="blog-meta">
                        <h4>
                          <a href="tech-single.html">
                            We are guests of ABC Design Studio - Vlog
                          </a>
                        </h4>
                      </div>
                      {/* end meta */}
                    </div>
                    {/* end blog-box */}
                    <hr className="invis" />
                    <div className="blog-box">
                      <div className="post-media">
                        <a href="tech-single.html">
                          <img
                            src="upload/tech_video_03.jpg"
                            alt
                            className="img-fluid"
                          />
                          <div className="hovereffect">
                            <span className="videohover" />
                          </div>
                          {/* end hover */}
                        </a>
                      </div>
                      {/* end media */}
                      <div className="blog-meta">
                        <h4>
                          <a href="tech-single.html">
                            Both blood pressure monitor and intelligent clock
                          </a>
                        </h4>
                      </div>
                      {/* end meta */}
                    </div>
                    {/* end blog-box */}
                  </div>
                  {/* end videos */}
                </div>
                {/* end widget */}
                <div className="widget">
                  <h2 className="widget-title">Popular Posts</h2>
                  <div className="blog-list-widget">
                    <div className="list-group">
                      <a
                        href="tech-single.html"
                        className="list-group-item list-group-item-action flex-column align-items-start"
                      >
                        <div className="w-100 justify-content-between">
                          <img
                            src="upload/tech_blog_08.jpg"
                            alt
                            className="img-fluid float-left"
                          />
                          <h5 className="mb-1">
                            5 Beautiful buildings you need..
                          </h5>
                          <small>12 Jan, 2016</small>
                        </div>
                      </a>
                      <a
                        href="tech-single.html"
                        className="list-group-item list-group-item-action flex-column align-items-start"
                      >
                        <div className="w-100 justify-content-between">
                          <img
                            src="upload/tech_blog_01.jpg"
                            alt
                            className="img-fluid float-left"
                          />
                          <h5 className="mb-1">
                            Let's make an introduction for..
                          </h5>
                          <small>11 Jan, 2016</small>
                        </div>
                      </a>
                      <a
                        href="tech-single.html"
                        className="list-group-item list-group-item-action flex-column align-items-start"
                      >
                        <div className="w-100 last-item justify-content-between">
                          <img
                            src="upload/tech_blog_03.jpg"
                            alt
                            className="img-fluid float-left"
                          />
                          <h5 className="mb-1">
                            Did you see the most beautiful..
                          </h5>
                          <small>07 Jan, 2016</small>
                        </div>
                      </a>
                    </div>
                  </div>
                  {/* end blog-list */}
                </div>
                {/* end widget */}
                <div className="widget">
                  <h2 className="widget-title">Recent Reviews</h2>
                  <div className="blog-list-widget">
                    <div className="list-group">
                      <a
                        href="tech-single.html"
                        className="list-group-item list-group-item-action flex-column align-items-start"
                      >
                        <div className="w-100 justify-content-between">
                          <img
                            src="upload/tech_blog_02.jpg"
                            alt
                            className="img-fluid float-left"
                          />
                          <h5 className="mb-1">
                            Banana-chip chocolate cake recipe..
                          </h5>
                          <span className="rating">
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                          </span>
                        </div>
                      </a>
                      <a
                        href="tech-single.html"
                        className="list-group-item list-group-item-action flex-column align-items-start"
                      >
                        <div className="w-100 justify-content-between">
                          <img
                            src="upload/tech_blog_03.jpg"
                            alt
                            className="img-fluid float-left"
                          />
                          <h5 className="mb-1">
                            10 practical ways to choose organic..
                          </h5>
                          <span className="rating">
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                          </span>
                        </div>
                      </a>
                      <a
                        href="tech-single.html"
                        className="list-group-item list-group-item-action flex-column align-items-start"
                      >
                        <div className="w-100 last-item justify-content-between">
                          <img
                            src="upload/tech_blog_07.jpg"
                            alt
                            className="img-fluid float-left"
                          />
                          <h5 className="mb-1">
                            We are making homemade ravioli..
                          </h5>
                          <span className="rating">
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                          </span>
                        </div>
                      </a>
                    </div>
                  </div>
                  {/* end blog-list */}
                </div>
                {/* end widget */}
                <div className="widget">
                  <h2 className="widget-title">Follow Us</h2>
                  <div className="row text-center">
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button facebook-button">
                        <i className="fa fa-facebook" />
                        <p>27k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button twitter-button">
                        <i className="fa fa-twitter" />
                        <p>98k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button google-button">
                        <i className="fa fa-google-plus" />
                        <p>17k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button youtube-button">
                        <i className="fa fa-youtube" />
                        <p>22k</p>
                      </a>
                    </div>
                  </div>
                </div>
                {/* end widget */}
                <div className="widget">
                  <div className="banner-spot clearfix">
                    <div className="banner-img">
                      <img src="upload/banner_03.jpg" className="img-fluid" />
                    </div>
                    {/* end banner-img */}
                  </div>
                  {/* end banner */}
                </div>
                {/* end widget */}
              </div>
              {/* end sidebar */}
            </div>
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </section>
    </>
  )
}
