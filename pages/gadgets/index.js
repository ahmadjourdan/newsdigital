import Link from 'next/link'
import PopularPosts from '../../components/home/popularPosts'
export default function Gadgets({ gadgetArticles }) {
  return (
    <>
      <div className="page-title lb single-wrapper">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <h2>
                <i className="fa fa-gears bg-orange" /> Gadgets
                <small className="hidden-xs-down hidden-sm-down">
                  Nulla felis eros, varius sit amet volutpat non.
                </small>
              </h2>
            </div>
            {/* end col */}
            <div className="col-lg-4 col-md-4 col-sm-12 hidden-xs-down hidden-sm-down">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link href="/">Home</Link>
                </li>
                <li className="breadcrumb-item active">Gadgets</li>
              </ol>
            </div>
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </div>
      {/* end page-title */}
      <section className="section">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12">
              <div className="sidebar">
                <div className="widget">
                  <div className="banner-spot clearfix">
                    <div className="banner-img">
                      <img src="upload/banner_07.jpg" className="img-fluid" />
                    </div>
                    {/* end banner-img */}
                  </div>
                  {/* end banner */}
                </div>
                {/* end widget */}
                <div className="widget">
                  <h2 className="widget-title">Trend Videos</h2>
                  <div className="trend-videos">
                    <div className="blog-box">
                      <div className="post-media">
                        <a href="tech-single.html">
                          <img
                            src="upload/tech_video_01.jpg"
                            className="img-fluid"
                          />
                          <div className="hovereffect">
                            <span className="videohover" />
                          </div>
                          {/* end hover */}
                        </a>
                      </div>
                      {/* end media */}
                      <div className="blog-meta">
                        <h4>
                          <a href="tech-single.html">
                            We prepared the best 10 laptop presentations for you
                          </a>
                        </h4>
                      </div>
                      {/* end meta */}
                    </div>
                    {/* end blog-box */}
                    <hr className="invis" />
                    <div className="blog-box">
                      <div className="post-media">
                        <a href="tech-single.html">
                          <img
                            src="upload/tech_video_02.jpg"
                            className="img-fluid"
                          />
                          <div className="hovereffect">
                            <span className="videohover" />
                          </div>
                          {/* end hover */}
                        </a>
                      </div>
                      {/* end media */}
                      <div className="blog-meta">
                        <h4>
                          <a href="tech-single.html">
                            We are guests of ABC Design Studio - Vlog
                          </a>
                        </h4>
                      </div>
                      {/* end meta */}
                    </div>
                    {/* end blog-box */}
                    <hr className="invis" />
                    <div className="blog-box">
                      <div className="post-media">
                        <a href="tech-single.html">
                          <img
                            src="upload/tech_video_03.jpg"
                            className="img-fluid"
                          />
                          <div className="hovereffect">
                            <span className="videohover" />
                          </div>
                          {/* end hover */}
                        </a>
                      </div>
                      {/* end media */}
                      <div className="blog-meta">
                        <h4>
                          <a href="tech-single.html">
                            Both blood pressure monitor and intelligent clock
                          </a>
                        </h4>
                      </div>
                      {/* end meta */}
                    </div>
                    {/* end blog-box */}
                  </div>
                  {/* end videos */}
                </div>
                {/* end widget */}

                <PopularPosts popularPosts={gadgetArticles} />

                <div className="widget">
                  <h2 className="widget-title">Follow Us</h2>
                  <div className="row text-center">
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button facebook-button">
                        <i className="fa fa-facebook" />
                        <p>27k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button twitter-button">
                        <i className="fa fa-twitter" />
                        <p>98k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button google-button">
                        <i className="fa fa-google-plus" />
                        <p>17k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button youtube-button">
                        <i className="fa fa-youtube" />
                        <p>22k</p>
                      </a>
                    </div>
                  </div>
                </div>
                {/* end widget */}
                <div className="widget">
                  <div className="banner-spot clearfix">
                    <div className="banner-img">
                      <img src="upload/banner_03.jpg" className="img-fluid" />
                    </div>
                    {/* end banner-img */}
                  </div>
                  {/* end banner */}
                </div>
                {/* end widget */}
              </div>
              {/* end sidebar */}
            </div>
            {/* end col */}
            <div className="col-lg-9 col-md-12 col-sm-12 col-xs-12">
              <div className="page-wrapper">
                <div className="blog-grid-system">
                  <div className="row">
                    {gadgetArticles.articles.map((data, index) => (
                      <div className="col-md-6" key={index}>
                        <div className="blog-box">
                          <div className="post-media">
                            <img src={data.urlToImage} className="img-fluid" />
                            <div className="hovereffect">
                              <span />
                            </div>
                          </div>
                          {/* end media */}
                          <div className="blog-meta big-meta">
                            <span className="color-orange">
                              <a href="tech-category-01.html">Gadgets</a>
                            </span>
                            <h4>
                              <Link
                                href={{
                                  pathname: '/articles/[id]',
                                  query: {
                                    detail: JSON.stringify(data),
                                    breadcrumb: 'Gadgets',
                                  },
                                }}
                                as={`articles/${data.title}`}
                              >
                                {data.title}
                              </Link>
                            </h4>
                            <p>{data.description}</p>
                            <small>{data.publishedAt}</small>
                            <small>
                              <a href="tech-author.html">by {data.author}</a>
                            </small>
                            <small>
                              <a href="tech-single.html">
                                <i className="fa fa-eye" /> 2887
                              </a>
                            </small>
                          </div>
                          {/* end meta */}
                        </div>
                        {/* end blog-box */}
                      </div>
                    ))}
                  </div>
                  {/* end row */}
                </div>
                {/* end blog-grid-system */}
              </div>
              {/* end page-wrapper */}
              <hr className="invis3" />
              <div className="row">
                <div className="col-md-12">
                  <nav aria-label="Page navigation">
                    <ul className="pagination justify-content-start">
                      <li className="page-item">
                        <a className="page-link" href="#">
                          1
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#">
                          2
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#">
                          3
                        </a>
                      </li>
                      <li className="page-item">
                        <a className="page-link" href="#">
                          Next
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
                {/* end col */}
              </div>
              {/* end row */}
            </div>
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </section>
    </>
  )
}

export const getStaticProps = async () => {
  const apiKey = 'e31c4651200f4400ab9afd8c8b20ce9f'
  const gadgetArticles = await fetch(
    `https://newsapi.org/v2/top-headlines?country=us&category=technology&apiKey=${apiKey}`
  ).then((r) => r.json())

  return {
    props: {
      gadgetArticles,
    },
  }
}
