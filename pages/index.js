import SlideShow from '../components/home/slideshow'
import RecentNews from '../components/home/recentNews'
import TrendVideos from '../components/home/trendVideos'
import PopularPosts from '../components/home/popularPosts'
import axios from 'axios'

export default function HomePage({ recentNews, popularPosts }) {
  return (
    <>
      <SlideShow slideShow={recentNews} />

      <section className="section">
        <div className="container">
          <div className="row">
            <RecentNews recentNews={recentNews} />

            <div className="col-lg-3 col-md-12 col-sm-12 col-xs-12">
              <div className="sidebar">
                <TrendVideos />
                <PopularPosts popularPosts={popularPosts} />

                <div className="widget">
                  <h2 className="widget-">Follow Us</h2>
                  <div className="row text-center">
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button facebook-button">
                        <i className="fa fa-facebook" />
                        <p>27k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button twitter-button">
                        <i className="fa fa-twitter" />
                        <p>98k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button google-button">
                        <i className="fa fa-google-plus" />
                        <p>17k</p>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                      <a href="#" className="social-button youtube-button">
                        <i className="fa fa-youtube" />
                        <p>22k</p>
                      </a>
                    </div>
                  </div>
                </div>
                <div className="widget">
                  <div className="banner-spot clearfix">
                    <div className="banner-img">
                      <img src="upload/banner_03.jpg" className="img-fluid" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export const getStaticProps = async () => {
  const apiKey = 'e31c4651200f4400ab9afd8c8b20ce9f'
  const [recentNews, popularPosts] = await Promise.all([
    fetch(
      `https://newsapi.org/v2/top-headlines?country=us&apiKey=${apiKey}`
    ).then((r) => r.json()),
    fetch(
      `https://newsapi.org/v2/everything?q=Apple&from=2021-11-05&sortBy=popularity&apiKey=${apiKey}`
    ).then((r) => r.json()),
  ])

  return {
    props: {
      recentNews,
      popularPosts,
    },
  }
}
